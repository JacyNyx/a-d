package ALD.LinkedList;

import ALD.LinkedList.LinkedStringList;

public class App {

	public static void main(String[] args) {

		LinkedStringList list = new LinkedStringList();
		list.add("Hello");
		list.add("World");
		list.add("!");
		
		System.out.println(list);
		
		String item2 = list.get(1);
		System.out.println("Item 2: " + item2);
		
		list.insert(1, "Campus");
		System.out.println(list);
		
		list.insert(0, "Test");
		System.out.println(list);
				
		list.remove(2);
		System.out.println(list);
		
		/*LinkedList<String> javaLinkedList = new LinkedList<String>();
		
		for(String item : javaLinkedList){
			System.out.println(item);
		}*/
		
		for(String item : list){
			System.out.println(item);
		}
		
		for(int i = 0; i < list.size(); i++){
			System.out.println(list.get(i));
		}
		
				
		/*java.util.Iterator<String> iterator = javaLinkedList.iterator();
		while(iterator.hasNext())
		{
			String item = iterator.next();
			System.out.println(item);
		}*/
	}

}
