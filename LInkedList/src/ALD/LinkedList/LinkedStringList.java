package ALD.LinkedList;

import java.util.Iterator;

public class LinkedStringList implements Iterable<String> {

	private ListItem firstItem;
	private ListItem lastItem;
	
	public void add(String value) {

		ListItem item = new ListItem(value);
		
		if (firstItem == null) {
			
			firstItem = item;
		}
		else {
			
			lastItem.setNext(item);
		}
		
		lastItem = item;
	}

	public void insert(int index, String value) {
		
		ListItem newItem = new ListItem(value);
	
		if (index == 0) {
			
			newItem.setNext(firstItem);
			firstItem = newItem;
			
			return;
		}
		
		ListItem previousItem = firstItem;
		int i = 0;
		
		while (previousItem != null){
			
			if (i == index - 1)
			{
				newItem.setNext(previousItem.getNext());
				previousItem.setNext(newItem);
				
				break;
			}
			
			previousItem = previousItem.getNext();
			i++;
		}
	}

	public void remove(int index) {
		
		ListItem previousItem = firstItem;
		int i = 0;
		
		while (previousItem != null){
			
			if (i == index - 1)
			{
				ListItem itemToRemove = previousItem.getNext();
				previousItem.setNext(itemToRemove.getNext());
								
				break;
			}
			
			previousItem = previousItem.getNext();
			i++;
		}
	}
	
	public int size(){
		
		int count = 0;
		ListItem item = firstItem;
		
		while (item != null)
		{
			count++;
			item = item.getNext();
		}
		
		return count;
	}
	
	public String get(int index){
		
		ListItem item = firstItem;
		int i = 0;
		
		while(item != null){
			
			if (i == index)
				return item.getValue();
			
			item = item.getNext();
			i++;
		}
		
		return null;
	}
	
	public String toString(){
		
		String result = "";
		ListItem item = firstItem;
		
		while(item != null){
			
			result = result + item.getValue() + " ";
			item = item.getNext();
		}
		
		return result;
	}

	@Override
	public Iterator<String> iterator() {
		return new LinkedStringListIterator(firstItem);
	}
}




