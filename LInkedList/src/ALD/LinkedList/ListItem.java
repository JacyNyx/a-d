package ALD.LinkedList;

public class ListItem {

	private String value;
	private ListItem nextItem;
	
	public ListItem(String value){
		this.value = value;
	}
	
	public String getValue(){
		return value;
	}

	public void setNext(ListItem item) {
		nextItem = item;		
	}
	
	public ListItem getNext(){
		return nextItem;
	}
}
