package ALD.LinkedList;

import java.util.Iterator;

public class LinkedStringListIterator implements Iterator<String> {

	private ListItem currentItem;
	
	public LinkedStringListIterator(ListItem firstItem) {
		currentItem = firstItem;
	}

	@Override
	public boolean hasNext() {
		return currentItem != null;
	}

	@Override
	public String next() {
		
		String value = currentItem.getValue();
		currentItem = currentItem.getNext();
		
		return value;
	}

}
