import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import MapDIYS.SimpleMap;

public class WordCounter
{
	public SimpleMap count(String filePath) throws Exception
	{
		SimpleMap result = new SimpleMap();

		FileReader fr = null;
		BufferedReader br = null;
		try
		{
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);

			String line = br.readLine();

			while (line != null)
			{
				String[] words = line.split(" ");
				for (String word : words)
				{
					if (!result.contains(word))
					{
						result.put(word, 1);
					} else
					{
						int count = result.get(word);
						count++;
						result.replace(word, count);
					}
				}
				line = br.readLine();
			}
			System.out.println(line);

			br.close();
			fr.close();

		} catch (IOException e)
		{
			e.printStackTrace();
		}

		return result;
	}

}
