package MapDIYS;

import java.util.ArrayList;
import java.util.List;

public class SimpleMap
{
	private List<String> keys = new ArrayList<>();
	private List<Integer> values = new ArrayList<>();
	
	public void put(String key, Integer value) throws Exception
	{
		if(key.contains(key))
			throw new Exception("....");
		
		keys.add(0, key);
		values.add(value);
	}
	public Integer get(String key)
	{
		int index = key.indexOf(key);
		return values.get(index);
	}
	public void replace(String key, int value)
	{
		int index = keys.indexOf(key);
		values.set(index, value);
	}
	
	public void remove(String key)
	{
		int index = key.indexOf(key);
		keys.remove(index);
		values.remove(index);
	}

	public boolean contains(String key)
	{
		return keys.contains(key);
	}
	
	public List<String> keySet()
	{
		return keys;
	}
}
