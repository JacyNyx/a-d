import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class Main
{

	public static void main(String[] args)
	{
//		erstellt eine Map mit Schl�ssel und Wert
		Map<Integer, String> map = new HashMap<Integer, String>();
		
//		bef�llt die Map an erster stelle der Key an zweiter der Wert zum Key
		map.put(3, "Bonjoure");
		map.put(1, "la vivre");
		map.put(4, "!");
		
		for (int key : map.keySet())
		{
			System.out.println("Key: " + key);
			String value = map.get(key);
			System.out.println("Value: " + value);
		}
		System.out.println();
		
		for (String value : map.values())
		{
			System.out.println("Value: " + value);
		}
//		ersetzt den wert mit dem Schl�ssel 1 mit dem angegebenen Inhalt
		map.replace(1, "Replaced");
		
		
		System.out.println();
		System.out.println("EntrySet");
		for (Entry<Integer, String> entry : map.entrySet())
		{
			System.out.println("Entry: " + entry);
		}
		

	}

}
