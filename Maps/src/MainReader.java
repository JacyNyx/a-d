import MapDIYS.SimpleMap;

public class MainReader
{

	public static void main(String[] args) throws Exception
	{
		WordCounter counter = new WordCounter();
		SimpleMap result = counter.count("WordCount-Sample.txt");

		for (String key : result.keySet())
		{
			int value = result.get(key);
			System.out.println(key + "\t" + "\t" + value);
		}
	}

}
