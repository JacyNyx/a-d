

import java.util.Stack;

public class TreePreOrder {
	 
	 
	public static class TreeNode
	{
		int data;
		TreeNode left;
		TreeNode right;
		TreeNode(int data)
		{
			this.data=data;
		}
	}
 
	// Recursive Solution
	public void preorder(TreeNode root) {
		if(root !=  null) {
			//Visit the node-Printing the node data  
			System.out.printf("%d ",root.data);
			preorder(root.left);
			preorder(root.right);
		}
	}
 
	// Iterative solution
	public void preorderIter(TreeNode root) {
 
		if(root == null)
			return;
 
		Stack<TreeNode> stack = new Stack<TreeNode>();
		stack.push(root);
 
		while(!stack.empty()){
 
			TreeNode n = stack.pop();
			System.out.printf("%d ",n.data);
 
 
			if(n.right != null){
				stack.push(n.right);
			}
			if(n.left != null){
				stack.push(n.left);
			}
 
		}
 
	}
}