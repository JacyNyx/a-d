
public class TreeApp
{

	public static void main(String[] args)
	{
		BinTree k = new BinTree(10);
		BinTree j = new BinTree(5);
		BinTree f = new BinTree(j, 3, k);
		BinTree e = new BinTree(8);
		BinTree b = new BinTree(e, 9, f);
		BinTree g = new BinTree(15);
		BinTree empty = new BinTree();
		BinTree c = new BinTree(empty, 19, g);
		BinTree a = new BinTree(b, 1, c);
		
		System.out.println("Wert der Wurzel: " + k.getRoot());
		System.out.println("schaut ob der Baum leer ist: " + k.isEmpty());

		System.out.print("Baum im Tiefendurchlauf: ");
		DFIterator it = a.dfIterator();
		while (it.hasNext())
		{
			System.out.print(it.next() + "/ ");
		}
		System.out.println();

		System.out.print("Baum im Breitendurchlauf: ");
		BFIterator it2 = a.bfIterator();
		while (it2.hasNext())
		{
			System.out.print(it2.next() + "/ ");
		}

		// Die Variablen repr�sentieren jeweils
		// den Teilbaum mit dem gleichnamigen
		// Wurzelknoten.
	}
}

// Zusammenfassung
// B�ume sind eine wichtige Datenstruktur in der Informatik
// Bin�re B�ume k�nnen in Java implementiert werden als Verallgemeinerung
// der einfach verketteten Listen mit mehreren Nachfolgerverweisen.
// Viele Operationen auf bin�ren B�umen werden rekursiv definiert.
// In Java kann man rekursiv definierte Funktionen auf B�umen implementieren
// durch Weitergeben der Operation an die Knotenklasse oder
// durch Fallunterscheidung bzgl. des leeren Baums und rekursiven Aufruf
// der Selektoren getLeft() und getRight() von BinTree.
// Wichtige Arten des Baumdurchlaufs sind Tiefendurchlauf und
// Breitendurchlauf