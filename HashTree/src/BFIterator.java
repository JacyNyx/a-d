import java.util.LinkedList;

public class BFIterator
{
	// Liste der Wurzeln aller noch zu besuchender Teilb�ume,
	// in der Reihenfolge, in der sie besucht werden sollen.
	LinkedList<Node> toVisit;

	// Der Iterator besucht den Baum mit Wurzel n als
	// Breitendurchlauf.
	public BFIterator(Node n)
	{
		toVisit = new LinkedList<Node>();
		if (n != null)
		{
			toVisit.addFirst(n);
		}
	}

	public boolean hasNext()
	{
		return !toVisit.isEmpty();
	}

	public int next()
	{
		Node n = toVisit.removeFirst();
		Node left = n.getLeft();
		Node right = n.getRight();
		
		// Im Unterschied zum Tiefendurchlauf werden die beiden
		// Teilb�ume von n hier hinten angef�gt.
		if (left != null)
		{
			toVisit.addLast(left);
		}
		if (right != null)
		{
			toVisit.addLast(right);
		}
		return (int) n.getValue();
	}

}
