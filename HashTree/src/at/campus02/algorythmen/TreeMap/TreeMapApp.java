package at.campus02.algorythmen.TreeMap;

public class TreeMapApp
{

	public static void main(String[] args)
	{
		Tree tree = new Tree();
		
		tree.add("D");
		tree.add("F");
		tree.add("C");	
		tree.add("E");
		tree.add("A");
		tree.add("B");
		
		System.out.println("Preordert:");
		tree.preorder();
		System.out.println();
		
		System.out.println("Inordert:");
		tree.InOrder();
		System.out.println();
		
//		diese Methode sucht nach dem angegebenen Wert
//		und gibt den Weg aus wie sie den Baum durchläuft bis sie 
//		den gesuchten wert gefunden hat
		
		if(tree.searchPreorder("D"))
			System.out.println("Success!");
		else
			System.out.println("Search not found!");
		
		System.out.println();
		

	}

}
