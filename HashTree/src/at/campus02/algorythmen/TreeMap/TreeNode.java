package at.campus02.algorythmen.TreeMap;

public class TreeNode
{
	String value;
	TreeNode left;
	TreeNode right;
	
	public TreeNode(String value)
	{
		this.value = value;
	}
	
//	Fragt je links und rechts ab wo Platz ist und f�gt dementsprechend den neuen Wert
//	im Baum hinzu damit ein Gleichgewicht gegeben ist
	public void add(String newValue)
	{
		if(newValue.compareTo(value) < 0)
		{
			if(left == null)
			left = new TreeNode(newValue);
			else
				left.add(newValue);
		
		}
		
		if(newValue.compareTo(value) > 0)
		{
			if(right == null)
			right = new TreeNode(newValue);
			else
				right.add(newValue);
		}
		
	}

	public void preorder()
	{
		System.out.println("Node: " +  value);
		
		if(left != null)
			left.preorder();
		
		if(right != null)
			right.preorder();
	}
	
//	sucht mithilfe der Preorder nach einem gesuchten wert
	public boolean searchPreorder(String pattern)
	{
		System.out.println("Node: " +  value);
		
		if(value.equals(pattern))
		{
			System.out.println("Found: " +  value);
			return true;
		}
		if(left != null)
		{
			boolean success = left.searchPreorder(pattern);
		
		if(success == true)
			return true;
		}
		if(right != null)
		{
			boolean success = right.searchPreorder(pattern);
		
		if(success == true)
			return true;
		}
		return false;
	}
	
	public TreeNode getLeft()
	{
		return left;
	}

	public TreeNode getRight()

	{
		return right;
	}
	
	public void InOrder()
	{
		if(left != null)
			left.InOrder();
		
		System.out.println("Node: " +  value);
		
		
		if(right != null)
			right.InOrder();
		
	}
}
