package at.campus02.algorythmen.TreeMap;

public class Tree
{
	private TreeNode root;

	public void add(String value)
	{
		if (root == null)
		{
			root = new TreeNode(value);
			return;
		}

		root.add(value);
	}
	
	public void preorder()
	{
		if(root != null)
			root.preorder();
		
	}
	
	public boolean searchPreorder(String pattern)
	{
		if(root != null)
			return root.searchPreorder(pattern);
		
		return false;
	}
	
	
	public void InOrder()
	{
		if(root != null)
			root.InOrder();
		
	}



}
