

import at.campus02.als.stack.SimpleStack;

public class App
{
public static void main(String [] args)
{
	SimpleStack Test = new SimpleStack();
	
	Test.push("Hallo 1");
	Test.push("Hallo 2");
	Test.push("Hallo 3");
	Test.push("Hallo 4");
	Test.push("Hallo 5");
	Test.push("Hallo 6");
	Test.push("Hallo 7");
	Test.push("Hallo 8");
	Test.push("Hallo 9");
	Test.push("Hallo 10");
	
	for (int i = 0; i < Test.getCount(); i++)
	{
		System.out.printf("%s   ", Test.get(i));
	}

	System.out.println(Test.size());
}
}
