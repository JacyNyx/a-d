package at.campus02.als.stack;

import java.util.Stack;

public class Main
{

	public static void main(String[] args)
	{
		
//		mit push wird etwas auf den Stapel gelegt
		Stack<String> stack = new Stack<String>();
		stack.push("erster");
		stack.push("zweiter");
		stack.push("dritter");
		
		System.out.println("Vorhandene Datein: " + stack.size());
		
		String peekValue = stack.peek();
		System.out.println("letzte Datei: " + peekValue);
		
//		value gibt die letzte abgelegte Datei an und Pop entfernt die letzte Datei vom Stapel
		String value = stack.pop();
		System.out.println("letzte Datei: " + value);
		
//		gibt die gr��e des Stapels an
		System.out.println("Vorhandene Datein: " + stack.size());
		
		

	}

}
