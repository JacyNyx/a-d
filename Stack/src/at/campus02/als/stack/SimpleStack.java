package at.campus02.als.stack;

import java.util.Arrays;

public class SimpleStack
{
	private String[] values;
	private int index;

	public SimpleStack()
	{
		values = new String[10];
		index = 0;
	}

	public void push(String value)
	{
		if (index >= values.length)
		{
			String[] newValues = Arrays.copyOf(values, values.length * 2);
			values = newValues;

			values[index] = value;
			index++;
		}
	}

	public String pop()
	{
		index--;
		String value = values[index];
		values[index] = null;

		return value;
	}

	public String peek()
	{
		return values[index - 1];
	}

	public int size()
	{
		return values.length;
	}
	
	public int getCount()
	{
		return index;
	}

	public String get(int counter)
	{
		return values[counter];
	}

}
