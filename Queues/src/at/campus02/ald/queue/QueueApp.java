package at.campus02.ald.queue;

public class QueueApp
{

	public static void main(String[] args)
	{
		SimpleQueue qualle = new SimpleQueue();

		System.out.println(qualle.isEmpty());

		qualle.offer("Giraffe");
		qualle.offer("Gibbon");
		qualle.offer("Gorilla");
		qualle.offer("Gnu");

		for (int i = 0; i < qualle.getCount(); i++)
		{
			System.out.printf("%s   ", qualle.get(i));
		}

		System.out.println();

		System.out.println(qualle.peek());
		qualle.poll();

		for (int i = 0; i < qualle.getCount(); i++)
		{
			System.out.printf("%s   ", qualle.get(i));
		}

		System.out.println();

	}
}