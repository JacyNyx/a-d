package at.campus02.ald.queue;

import java.util.ArrayList;

public class SimpleQueue
{
	ArrayList<String>	queue	= new ArrayList<String>();
	int					counter	= 0;

	public void offer(String value)
	{
		queue.add(value);
	}

	public String poll()
	{
		return queue.remove(counter);
	}

	public boolean isEmpty()
	{
		if (!queue.isEmpty())
		{
			return false;
		}
		return true;
	}

	public int getCount()
	{
		return queue.size();
	}

	public String get(int i)
	{
		return queue.get(i);
	}

	public String peek()
	{
		return queue.get(counter);
	}
}