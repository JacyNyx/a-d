package at.campus02.ald.queue;

import java.util.PriorityQueue;

public class MainQueue
{

	public static void main(String[] args)
	{
		PriorityQueue<String> values = new PriorityQueue<String>();

		values.offer("Bongos");
		values.offer("Elephant");
		values.offer("Palmtree");

		System.out.println("Size:\t" + values.size());
		System.out.println("Queue:\t" + values);
		System.out.println("Next:\t" + values.peek());

		values.poll();

		System.out.println("Size:\t" + values.size());
		System.out.println("Queue: \t" + values);
		System.out.println("Next:\t" + values.peek());
	}

}