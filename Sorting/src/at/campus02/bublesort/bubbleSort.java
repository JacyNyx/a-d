package at.campus02.bublesort;

import java.util.Arrays;

public class bubbleSort
{
	public static void main(String[] args)
	{

		int[] unsort =
		{ 3, 5, 1, 7, 4};

		System.out.println("Unsortiert: " + "\t");
		for (int i = 0; i < unsort.length; i++)
		{
			System.out.print(unsort[i] + ", ");
		}

		System.out.println();
		System.out.println();

		int[] sort = bubblesort(unsort);

		System.out.println("Sortiert: " + "\t");
		for (int i = 0; i < sort.length; i++)
		{
			System.out.print(sort[i] + ", ");

		}
		System.out.println();
	}

	public static int[] bubblesort(int[] sort)
	{
		int temp;

		for (int i = 1; i < sort.length; i++)
		{
			for (int j = 0; j < sort.length - i; j++)
			{
				if (sort[j] > sort[j + 1])
				{
					temp = sort[j];
					sort[j] = sort[j + 1];
					sort[j + 1] = temp;
				}
				System.out.println(Arrays.toString(sort));

			}
		}
		System.out.println();
		return sort;
	}

}
