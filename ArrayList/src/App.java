

import at.campus02.ald.SimpleArrayList;

public class App {

	public static void main(String[] args) {

		SimpleArrayList list = new SimpleArrayList();
		list.add("Hello");
		list.add("World");
		list.add("3");
		list.add("4");
		list.add("5");
		list.add("6");
		list.add("7");
		list.add("8");
		list.add("9");
		list.add("10");
		
		for(int i = 0; i < list.getCount(); i++) { 
			//System.out.println(list.get(i));
		}
		
		//System.out.println("remove index 3");
		list.remove(3);

		for(int i = 0; i < list.getCount(); i++) { 
			//System.out.println(list.get(i));
		}
		
		
		System.out.println("insert 'new value'");
		list.add("new value");
		list.insert(1, "inserted value");
		
		for(int i = 0; i < list.getCount(); i++) { 
			System.out.println(list.get(i));
		}
		
	}

}
