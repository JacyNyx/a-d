package at.campus02.ald;

import java.util.Arrays;

public class SimpleArrayList {

	private String[] values;
	private int count;
	
	public SimpleArrayList() {
		values = new String[5];
		count = 0;
	}
	
	public void add(String value) {

		extendArrayIfNeeded();
			
		values[count] = value;
		count++;
	}
	
	public void remove(int index) {
		
		for(int i = index; i < values.length - 1; i++) {
			
			values[i] = values[i + 1];
		}
		
		values[values.length - 1] = null;
		count--;
	}
	
	public void insert(int index, String value) {

		extendArrayIfNeeded();
		
		for(int i = values.length - 1; i > index; i--) {
			
			values[i] = values[i - 1];
		}
		
		values[index] = value;
		count++;
	}
	
	private void extendArrayIfNeeded() {
		
		if (count >= values.length) {
			
			String[] newValues = Arrays.copyOf(values, values.length * 2);
			values = newValues;
			
			System.out.println("new length: " + values.length);
			
			//String[] newValues2 = new String[values.length * 2];
			//System.arraycopy(value, 0, newValues2, 0, values.length);
		}
	}
	
	public String get(int index) {
		return values[index];
	}
	
	public int getCount() {
		return count;
	}
}
